/************************************************/
/*                                              */
/* Class CameraControl                          */
/*                                              */
/* Inherits from: Base class                    */
/*                                              */
/* Purpose:  allow keyboard input to control    */
/*	     camera				*/
/*                                              */
/* Author:   michael branton                    */
/*                                              */
/* Date:     Sept 19, 2015                      */
/*                                              */
/*  
/*                                              */
/************************************************/

// constructor
function CameraControl(camera)
{
	this.speed = 0.5;			// default speed of all movement
	this.deltas = 0.001;		// speed increase/decreas increment
	this.camera=camera;		// the camera to be controlled
	this.phi=Math.PI/2;		// angle from the vertical
        this.theta=-Math.PI/2;		// angle in the x-z plane

        this.upVector=new THREE.Vector3(0,Math.cos(this.phi),0);	// up direction for the camera

        this.directionVector=new THREE.Vector3(	Math.cos(this.theta),	// direction the camera is looking
						0,
						Math.sin(this.theta));

        this.lookAtPoint=new THREE.Vector3(	this.camera.position.x+this.directionVector.x,	
						this.camera.position.y+this.directionVector.y,
						this.camera.position.z+this.directionVector.z);	// point the
												// camera is
												// looking at
        this.camera.lookAt(this.lookAtPoint);

	// bind to keydown event
	document.addEventListener("keydown", bind( this, this.onKeyDown ), false);

}

// response to keyboard events
CameraControl.prototype.onKeyDown = function(event)
{ 
	var keyCode = event.which;

        if(keyCode==65) //a  move left
        {
                this.camera.position.x += this.speed*this.directionVector.z;
                this.camera.position.z -= this.speed*this.directionVector.x;
                this.lookAtPoint.x += this.speed*this.directionVector.z;
                this.lookAtPoint.z -= this.speed*this.directionVector.x;
        }
        else if(keyCode==68) //d  move right
        {
                this.camera.position.x -= this.speed*this.directionVector.z;
                this.camera.position.z += this.speed*this.directionVector.x;
                this.lookAtPoint.x -= this.speed*this.directionVector.z;
                this.lookAtPoint.z += this.speed*this.directionVector.x;
        }
        else if(keyCode==83) //s move back
        {
                this.camera.position.x -= this.speed*this.directionVector.x;
		this.camera.position.y -= this.speed*this.directionVector.y;
                this.camera.position.z -= this.speed*this.directionVector.z;
                this.lookAtPoint.x -= this.speed*this.directionVector.x;
		this.lookAtPoint.y -= this.speed*this.directionVector.y;
                this.lookAtPoint.z -= this.speed*this.directionVector.z;
        }
        else if(keyCode==87) //w move forward
        {
                this.camera.position.x += this.speed*this.directionVector.x;
		this.camera.position.y += this.speed*this.directionVector.y;
                this.camera.position.z += this.speed*this.directionVector.z;
                this.lookAtPoint.x += this.speed*this.directionVector.x;
		this.lookAtPoint.y += this.speed*this.directionVector.y;
                this.lookAtPoint.z += this.speed*this.directionVector.z;
        }
        else if(keyCode==37) // left arrow  rotate left
        {
                this.theta-=this.speed;

                this.directionVector.x=Math.sin(this.phi)*Math.cos(this.theta);
                this.directionVector.y=Math.cos(this.phi)
                this.directionVector.z=Math.sin(this.phi)*Math.sin(this.theta);
                this.lookAtPoint.x=this.camera.position.x+this.directionVector.x;
		this.lookAtPoint.y=this.camera.position.y+this.directionVector.y;
                this.lookAtPoint.z=this.camera.position.z+this.directionVector.z;
	}
        else if(keyCode==39) // right arrow  rotate right
        {
                this.theta+=this.speed;

                this.directionVector.x=Math.sin(this.phi)*Math.cos(this.theta);
                this.directionVector.y=Math.cos(this.phi)
                this.directionVector.z=Math.sin(this.phi)*Math.sin(this.theta);
                this.lookAtPoint.x=this.camera.position.x+this.directionVector.x;
		this.lookAtPoint.y=this.camera.position.y+this.directionVector.y;
                this.lookAtPoint.z=this.camera.position.z+this.directionVector.z;

        }
        else if(keyCode==82)  // r move up
        {
                this.lookAtPoint.y+=this.speed;
                this.camera.position.y+=this.speed;
        }
        else if(keyCode==70) // f move down
        {
                this.lookAtPoint.y-=this.speed;
                this.camera.position.y-=this.speed;
        }
	else if(keyCode==187)  // plus increase speed
        {
                this.speed+=this.deltas;
        }
        else if(keyCode==189) // minus decrease speed
        {
                this.speed-=this.deltas;
		if(this.speed<=0) this.speed=0;
        }
        this.camera.lookAt(this.lookAtPoint);
}

// only newest versions of javascript have bind
// funfunction built-in, so we provide one here
function bind( scope, fn )
{

	return function()
	{
		fn.apply( scope, arguments );
	};

};

