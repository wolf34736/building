// purpose: create a building
//
// requires: three.js, RequestAnimationFrame.js, CameraControl.js
//
// author: tgwright
//
// date: oct 1, 2015

var     renderer = null,
        scene = null,
    	camera = null,
    	cameraControl = null,

    	backwall = null,						//house
		leftWall = null,
		rightWall = null,						
		frontWall = null,
		floor = null,
		ceiling = null,
		door = null,
		
		artifact = null,						//artifact

    	dlight=null,                            // a directional light
    	alight=null,                            // ambient light
		hlight=null;							// hemisphere light

THREE.ImageUtils.crossOrigin = "Anonymous";

function onLoad()
{
	// Grab our container div
    var container = document.getElementById("container");

    // Create the Three.js renderer, add it to our div
	renderer = new THREE.WebGLRenderer( { antialias: true } );
	renderer.setSize(container.offsetWidth, container.offsetHeight);
	renderer.setClearColor( 0x00688b );
	container.appendChild( renderer.domElement );

	// Create a new Three.js scene
 	scene = new THREE.Scene();

	// Put in a camera
    camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 1, 1000 );
    camera.position.set( 0, 0, 4 );
	scene.add(camera);

	cameraControl=new CameraControl(camera);

     // Create a directional light to show off the object
	dlight = new THREE.DirectionalLight( 0x0b0b0b);
	camera.add( dlight );

	// ambient light
	alight = new THREE.AmbientLight(0x0b0b0b);
	scene.add( alight );

	// hemisphere light
	hlight = new THREE.HemisphereLight(0xffffbb, 0x080820, 1);
	scene.add( hlight );

	//build house
	buildHouse(100,20,2);
	
    // Run our render loop
    run();
}

function run()
{
	// Render the scene
	renderer.render( scene, camera );

    // Ask for another frame
    requestAnimationFrame(run);
}

	
//create a house with a floor
//calls build statue based on wallWidth and wallHeight and walThick
//calls build table based on wallThick and wallHeight
function buildHouse(wallWidth, wallHeight, wallThick)
{
	var wmaterial = new THREE.MeshPhongMaterial( { map: THREE.ImageUtils.loadTexture('./bamboo.jpg'),
													side:THREE.DoubleSide } );
	var gmaterial = new THREE.MeshLambertMaterial( { map: THREE.ImageUtils.loadTexture('./window.jpg'),
													blending: THREE.NormalBlending,
													color:0x94dbff,
				   									transparent: true,
													opacity: .4 } );


	//back
	var geometry = new THREE.CubeGeometry( wallWidth, wallHeight, wallThick );
	this.backwall = new THREE.Mesh( geometry, wmaterial);
	this.backwall.position.z -= wallWidth *(3/2);
	scene.add(backwall);

	//left
	geometry = new THREE.CubeGeometry( wallThick, wallHeight, wallWidth);
	this.leftwall = new THREE.Mesh( geometry, wmaterial);
	this.leftwall.position.x -= wallWidth/2;
	this.leftwall.position.z += wallWidth/2;
	this.backwall.add(leftwall);

	//right
	geometry = new THREE.CubeGeometry( wallThick, wallHeight, wallWidth);
	this.rightwall = new THREE.Mesh( geometry, wmaterial);
	this.rightwall.position.z += wallWidth/2;
	this.rightwall.position.x += wallWidth/2;
	this.backwall.add(rightwall);

/*	//front
	geometry = THREE.CubeGeometry( 4*wallWidth/5, wallHeight, wallThick);
	this.frontwall = new THREE.Mesh(geometry, wmaterial);
	this.frontwall.position.z += wallWidth - wallThick;
	this.frontwall.position.x += wallWidth/9;
	this.backwall.add(frontwall);
*/
	//floor
	geometry = THREE.CubeGeometry( wallWidth, wallThick, wallWidth);
	this.floor = new THREE.Mesh(geometry, wmaterial);
	this.floor.position.y -= wallHeight/2;
	this.floor.position.z += wallWidth/2;
	this.backwall.add(floor);

	//ceiling
	geometry = THREE.CubeGeometry(wallWidth, wallThick, wallWidth);
	this.ceiling = new THREE.Mesh(geometry, wmaterial);
	this.ceiling.position.y += wallHeight/2;
	this.ceiling.position.z += wallWidth/2;
	this.backwall.add(ceiling);

	//door
	geometry = THREE.CubeGeometry(wallWidth/5, wallHeight, wallThick);
	this.door = new THREE.Mesh(geometry, gmaterial);
	this.door.position.x -= wallWidth/2;
//	this.frontwall.add(door);
	
	buildStatue(wallWidth, wallHeight, wallThick);
	//buildTable(wallHeight, wallHeight/3, wallThick*6);
}

//build a statue outside the door
function buildStatue(wallWidth, wallHeight, wallThick){
	var geometry = THREE.CubeGeometry(wallWidth/8, wallHeight, wallThick/2);
	var smaterial = new THREE.MeshPhongMaterial( { map: THREE.ImageUtils.loadTexture('./Solo.jpg'),
													side:THREE.DoubleSide } );
	var surface = new THREE.Mesh( geometry, smaterial);
	surface.position.x -= wallWidth/2;
	surface.position.z -= wallWidth/4;
	scene.add(surface);
}

//builds a table in the house
function buildTable(tableWidth, tableHeight, tableLength){
	var geometry = THREE.CubeGeometry(tableWidth, 2, tableLength);
	var tmaterial = new THREE.MeshPhongMaterial({map: THREE.ImageUtils.loadTexture('./table.jpg'),
													side: THREE.DoubleSide } );
	var table = new THREE.Mesh(geometry, tmaterial);
	table.position.y += tableHeight;
	this.floor.add(table);

	}
